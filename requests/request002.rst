request002: inoltro dati di audit interno dal Fruitore all'Erogatore
====================================================================

**dateRequest**: 15/12/2022

**subjetRequest**: Agenzia delle Entrate

**descriptionRequest**


L’Agenzia delle Entrate, per dare seguito alle prescrizioni disposte dal Garante per la Protezione dei Dati Personali alla stessa Agenzia delle Entrate relativamente all’Anagrafe Tributaria, ha evidenziato l’esigenza di individuare delle soluzioni specifiche relativamente alla modalità per permettere ai fruitori degli e-service dell'Agenzia delle Entrate di dare evidenza, in tecnologia REST, della:

- identificazione della persona fisica (User) quale operatore a cui è imputabile l'invocazione dell'e-service;
- identificazione della postazione (IP-Device) dalla quale la persona fisica(User) realizza l'azione di invocazione dell'e-service;



