# Linee Guida sull'interoperabilità tecnica delle Pubbliche Amministrazioni 


Il presente repo è reso disponibile per dare seguito alla condivisione degli aggiornamenti alle Linee Guida attuando quanto stabilito al punto b) del comma 3-ter Art. 73 del decreto legislativo 7 marzo 2005, n. 82 (Codice dell'Amministrazione Digitale) cosi come previsto al Capitolo 6 - Pattern e profili di interoperabilità delle stesse Linee Guida.

- Il folder **requests** riporta le esigenze espresse dalle Pubbliche Amministrazioni pervenute ad AgID o individuate dalla stessa.
- Il folder **proposals** riporta le proposte di pattern di interoperabilità o profili di interoperabilità individuati per dare seguito alle esigenze di cui al precedente punto.

Si invitano le Pubbliche Amministrazioni interessate ad utilizzare gli strumenti del presente repo (**Issues** or **Pull reuests**) per partecipare alla concertazione delle proposte di pattern di interoperabilità o profili di interoperabilità.
